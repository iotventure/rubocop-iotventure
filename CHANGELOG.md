# Changelog

## [0.3.0] - 2023-06-01

- BU-64 Add AdditionalProperties cop

## [0.2.1] - 2023-04-21

- Fix SchemaDefinitionPerResponse cop for 204 No Content responses

## [0.2.0] - 2022-05-12

- FB-111 Add SchemaDefinitionPerResponse cop

## [0.1.1] - 2022-04-18

- Fix error for nonexistent response definition

## [0.1.0] - 2022-04-18

- Initial release

- Add DuplicateResponseCode cop

- Add SaveRequestExample cop
