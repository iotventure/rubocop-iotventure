# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Iotventure::DuplicateResponseCode do
  subject(:cop) { described_class.new }

  it 'registers an offense for duplicated responses on same level' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 400, 'response description 1' do
          end

          response 400, 'response description 2' do
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Response Code `400` is defined at both (string):3 and (string):6.
          end
        end
      end
    RUBY
  end

  it 'registers an offense for duplicated responses on same nested level' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          context 'context 1' do
            response 400, 'response description 1' do
            end
          end

          context 'context 2' do
            response 400, 'response description 2' do
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Response Code `400` is defined at both (string):4 and (string):9.
            end
          end
        end
      end
    RUBY
  end

  it 'registers an offense for duplicated responses on different levels' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 400, 'response description 1' do
          end

          context 'context 2' do
            response 400, 'response description 2' do
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Response Code `400` is defined at both (string):3 and (string):7.
            end
          end
        end
      end
    RUBY
  end

  it 'registers an offense for response code definition in `times`` loop' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          2.times do
            response 400, 'response description' do
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Response definition at (string):4 is defined inside loop at (string):3
            end
          end
        end
      end
    RUBY
  end

  it 'registers an offense for response code definition in `each` loop' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          [:a, :b].each do
            response 400, 'response description' do
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Response definition at (string):4 is defined inside loop at (string):3
            end
          end
        end
      end
    RUBY
  end

  it 'does not register an offense for different response codes' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 400, 'response description 1' do
          end

          response 401, 'response description 2' do
          end
        end
      end
    RUBY
  end

  it 'does not register an offense for different routes with same response code' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 400, 'response description 1' do
          end
        end

        get 'Get' do
          response 400, 'response description 2' do
          end
        end
      end
    RUBY
  end

  it 'does not register an offense for different paths with same response code' do
    expect_no_offenses(<<~RUBY)
      describe 'Devices API' do
        path '/path' do
          post 'Post' do
            response 400, 'response description' do
            end
          end
        end

        path '/other_path' do
          post 'Post' do
            response 400, 'response description' do
            end
          end
        end
      end
    RUBY
  end

  it 'does not register an offense if loop is around route definition' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        [:a, :b].each do
          post 'Post' do
            response 400, 'response description' do
            end
          end
        end
      end
    RUBY
  end

  it 'does not register an offense for malformed test with wrong http verb (just to make this cop more stable)' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        update 'Update' do
          response 400, 'response description' do
          end
        end
      end
    RUBY
  end
end
