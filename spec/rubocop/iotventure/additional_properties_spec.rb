# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Iotventure::AdditionalProperties do
  subject(:cop) { described_class.new }

  it 'registers an offense for missing additional properties' do
    expect_offense(<<~RUBY)
      {
      ^ Schema is missing additionalProperties. Please add it to the schema at (string):1
        type: :object,
        properties: {
          foo: { type: :string },
          bar: { type: :boolean }
        }
      }
    RUBY
  end

  it 'registers an offense if additional properties is set to true' do
    expect_offense(<<~RUBY)
      {
      ^ Schema has additionalProperties set to true. Please set it to false or define a rule at (string):1
        type: :object,
        properties: {
          foo: { type: :string },
          bar: { type: :boolean }
        },
        additionalProperties: true
      }
    RUBY
  end

  it 'registers an offense if additional properties is set to empty hash' do
    expect_offense(<<~RUBY)
      {
      ^ Schema has additionalProperties set to true. Please set it to false or define a rule at (string):1
        type: :object,
        properties: {
          foo: { type: :string },
          bar: { type: :boolean }
        },
        additionalProperties: {}
      }
    RUBY
  end

  it 'registers an offense for missing additional properties when using pattern properties' do
    expect_offense(<<~RUBY)
      {
      ^ Schema is missing additionalProperties. Please add it to the schema at (string):1
        type: :object,
        patternProperties: {
          "^foo": { type: :string },
          "^bar": { type: :boolean }
        }
      }
    RUBY
  end

  it 'registers an offense for missing additional properties for object declared with string' do
    expect_offense(<<~RUBY)
      {
      ^ Schema is missing additionalProperties. Please add it to the schema at (string):1
        type: "object",
        properties: {
          foo: { type: :string },
          bar: { type: :boolean }
        }
      }
    RUBY
  end

  it 'does not register an offense when additional properties are present' do
    expect_no_offenses(<<~RUBY)
      {
        type: :object,
        properties: {
          foo: { type: :string },
          bar: { type: :boolean }
        },
        additionalProperties: false
      }
    RUBY
  end

  it 'does not register an offense when additional properties are present when using pattern properties' do
    expect_no_offenses(<<~RUBY)
      {
        type: :object,
        patternProperties: {
          "^foo": { type: :string },
          "^bar": { type: :boolean }
        },
        additionalProperties: false
      }
    RUBY
  end

  it 'does not register an offense when additional properties are present for object declared with string' do
    expect_no_offenses(<<~RUBY)
      {
        type: "object",
        properties: {
          foo: { type: :string },
          bar: { type: :boolean }
        },
        additionalProperties: false
      }
    RUBY
  end

  it 'does not register an offense for more complex additionalProperties schema' do
    expect_no_offenses(<<~RUBY)
      {
        type: :object,
        properties: {
          foo: { type: :string },
          bar: { type: :boolean }
        },
        additionalProperties: {
          type: :string
        }
      }
    RUBY
  end
end
