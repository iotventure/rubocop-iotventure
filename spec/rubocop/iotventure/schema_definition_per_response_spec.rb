# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Iotventure::SchemaDefinitionPerResponse do
  subject(:cop) { described_class.new }

  it 'registers an offense for schema declaration inside context block' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
            context 'context 1' do
              schema '$ref' => '#/components/schemas/object1'
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for #/components/schemas/object1 is defined at (string):5, but should be defined immediately after response declaration at (string):3.
            end
          end
        end
      end
    RUBY
  end

  it 'registers an offense for schema declaration inside context block with additional code' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
            context 'context 1' do
              schema '$ref' => '#/components/schemas/object1'
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for #/components/schemas/object1 is defined at (string):5, but should be defined immediately after response declaration at (string):3.

              it_behaves_like 'some behaviour'
            end
          end
        end
      end
    RUBY
  end

  it 'registers an offense for composite schema declaration inside context block' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
            context 'context 1' do
              schema oneOf: [{ '$ref' => '#/components/schemas/error_object' },
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for composite schema declaration is defined at (string):5, but should be defined immediately after response declaration at (string):3.
                            { '$ref' => '#/components/schemas/errors_object' }]
            end
          end
        end
      end
    RUBY
  end

  it 'registers offense for duplicate schema declaration' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition is defined both at (string):4 and (string):5 for response declaration at (string):3.
            schema '$ref' => '#/components/schemas/object1'
            schema '$ref' => '#/components/schemas/object2'
          end
        end
      end
    RUBY
  end

  it 'registers offense for duplicate schema declaration when one is composite' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition is defined both at (string):4 and (string):5 for response declaration at (string):3.
            schema '$ref' => '#/components/schemas/object1'
            schema oneOf: [{ '$ref' => '#/components/schemas/error_object' },
                          { '$ref' => '#/components/schemas/errors_object' }]
          end
        end
      end
    RUBY
  end

  it 'registers two offenses for duplicate schema declaration when one is inside context block' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition is defined both at (string):4 and (string):7 for response declaration at (string):3.
            schema '$ref' => '#/components/schemas/object1'

            context 'context 1' do
              schema '$ref' => '#/components/schemas/object2'
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for #/components/schemas/object2 is defined at (string):7, but should be defined immediately after response declaration at (string):3.
            end
          end
        end
      end
    RUBY
  end

  it 'registers three offenses for duplicate schema declaration inside two context blocks' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition is defined both at (string):5 and (string):9 for response declaration at (string):3.
            context 'context 1' do
              schema '$ref' => '#/components/schemas/object1'
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for #/components/schemas/object1 is defined at (string):5, but should be defined immediately after response declaration at (string):3.
            end

            context 'context 2' do
              schema '$ref' => '#/components/schemas/object2'
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for #/components/schemas/object2 is defined at (string):9, but should be defined immediately after response declaration at (string):3.
            end
          end
        end
      end
    RUBY
  end

  it 'registers offense for schema declaration outside of response block' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          schema '$ref' => '#/components/schemas/object1'
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for #/components/schemas/object1 is outside of response declaration.

          response 200, 'response description' do
            schema '$ref' => '#/components/schemas/object1'
          end
        end
      end
    RUBY
  end

  it 'registers offense for composite schema declaration outside of response block' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          schema oneOf: [{ '$ref' => '#/components/schemas/error_object' },
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for composite schema declaration is outside of response declaration.
                        { '$ref' => '#/components/schemas/errors_object' }]

          response 200, 'response description' do
            schema '$ref' => '#/components/schemas/object1'
          end
        end
      end
    RUBY
  end

  it 'registers offense for response without schema declaration' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition is missing for response declaration at (string):3.
            it_behaves_like 'some behaviour'
          end
        end
      end
    RUBY
  end

  it 'does not register an offense for missing schema declaration inside 204 no content' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 204, 'response description' do
          end
        end
      end
    RUBY
  end

  it 'does not register an offense for correct schema declaration' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
            schema '$ref' => '#/components/schemas/object1'
          end
        end
      end
    RUBY
  end

  it 'does not register an offense for correct schema declaration with additional code' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
            schema '$ref' => '#/components/schemas/object1'

            it_behaves_like 'some behaviour'
          end
        end
      end
    RUBY
  end

  it 'does not register an offense for correct composite schema declaration' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
            schema oneOf: [{ '$ref' => '#/components/schemas/error_object' },
                            { '$ref' => '#/components/schemas/errors_object' }]
          end
        end
      end
    RUBY
  end

  it 'registers an offense for schema declaration inside 204 no content' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 204, 'response description' do
            schema '$ref' => '#/components/schemas/object1'
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for #/components/schemas/object1 is defined at (string):4, but 204 response should not have schema.
          end
        end
      end
    RUBY
  end

  it 'registers an offense for composite schema declaration inside 204 no content' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 204, 'response description' do
            schema oneOf: [{ '$ref' => '#/components/schemas/error_object' },
            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Schema definition for composite schema declaration is defined at (string):4, but 204 response should not have schema.
                            { '$ref' => '#/components/schemas/errors_object' }]
          end
        end
      end
    RUBY
  end
end
