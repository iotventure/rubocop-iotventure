# frozen_string_literal: true

RSpec.describe RuboCop::Cop::Iotventure::SaveRequestExample do
  subject(:cop) { described_class.new }

  it 'registers an offense when there is no arguments hash for response' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          parameter name: :param1, in: :body

          response 200, 'response description' do
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Request example `:param1` is missing at (string):5.
          end
        end
      end
    RUBY
  end

  it 'registers an offense when there arguments hash does not contain request example' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          parameter name: :param1, in: :body

          response 200, 'response description', other_argument: :abc do
          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Request example `:param1` is missing at (string):5.
          end
        end
      end
    RUBY
  end

  it 'registers an offense when save_request_example is misnamed' do
    expect_offense(<<~RUBY)
      path '/path' do
        post 'Post' do
          parameter name: :param1, in: :body

          response 200, 'response description', save_request_example: :param2 do
                                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Request example `:param2` is misnamed at (string):5.It should be `:param1`.
          end
        end
      end
    RUBY
  end

  it 'does not register an offense when save_request_example is correct' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          parameter name: :param1, in: :body

          response 200, 'response description', save_request_example: :param1 do
          end
        end
      end
    RUBY
  end

  it 'does not register an offense when there is no parameter and no save_request_example' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          response 200, 'response description' do
          end
        end
      end
    RUBY
  end

  it 'does not register an offense when there is no parameter in body and no save_request_example' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          parameter name: :param1, in: :query

          response 200, 'response description' do
          end
        end
      end
    RUBY
  end

  it 'does not register an offense when there is no successful response definition' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          parameter name: :param1, in: :body

          response 400, 'response description' do
          end
        end
      end
    RUBY
  end

  it 'does not register an offense when there is no response definition at all' do
    expect_no_offenses(<<~RUBY)
      path '/path' do
        post 'Post' do
          parameter name: :param1, in: :body
        end
      end
    RUBY
  end
end
