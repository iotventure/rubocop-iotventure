# frozen_string_literal: true

require 'rubocop'

require_relative 'rubocop/iotventure'
require_relative 'rubocop/iotventure/version'
require_relative 'rubocop/iotventure/inject'

RuboCop::Iotventure::Inject.defaults!

require_relative 'rubocop/cop/iotventure_cops'
