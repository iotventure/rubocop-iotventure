# frozen_string_literal: true

module RuboCop
  module Iotventure
    VERSION = '0.3.0'
  end
end
