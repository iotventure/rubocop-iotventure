# frozen_string_literal: true

require_relative 'iotventure/duplicate_response_code'
require_relative 'iotventure/save_request_example'
require_relative 'iotventure/schema_definition_per_response'
require_relative 'iotventure/additional_properties'