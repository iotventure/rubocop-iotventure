# frozen_string_literal: true

module RuboCop
  module Cop
    module Iotventure
      # This cop checks for missing request examples
      # (only needed when params in query).
      #
      # @example
      #
      #   # bad
      #
      #   parameter name: :param1, in: :body
      #   response 200, 'response description' {}
      #
      # @example
      #
      #   # bad
      #
      #   parameter name: :param1, in: :body
      #   response 200, 'response description', save_request_example: :param2 {}
      #
      #
      # @example
      #
      #   # good
      #
      #   parameter name: :param1, in: :body
      #   response 200, 'response description', save_request_example: :param1 {}
      #
      #
      # @example
      #
      #   # good
      #
      #   response 200, 'response description' {}
      #
      #
      class SaveRequestExample < Base
        MISSING_MSG = 'Request example `:%<param_name>s` is missing at %<current>s.'
        MISNAMED_MSG = 'Request example `:%<wrong_param_name>s` is misnamed at %<current>s.'\
                       'It should be `:%<correct_param_name>s`.'

        # @!method body_param_name?(node)
        def_node_matcher :body_param_name, <<~PATTERN
          (begin
            <(send nil? :parameter
              (hash
                <(pair
                  (sym :name)
                  (sym $_)
                )
                (pair
                  (sym :in)
                  (sym :body)
                )... >
            )) ... >
          )
        PATTERN

        # @!method success_response(node)
        def_node_matcher :success_response, <<~PATTERN
          (block
            (send nil? :response
              (:int #success_response_code?)
              (:str _)
              $...
            )
            ...
          )
        PATTERN

        # @!method save_request_example_name(node)
        def_node_matcher :save_request_example_name, <<~PATTERN
          (hash
            <
              (pair
                (sym :save_request_example)
                (sym $_)
              )
              ...
            >
          )
        PATTERN

        def on_begin(node)
          body_param_name = body_param_name(node)
          return unless body_param_name

          check_for_wrong_save_request_example(node, body_param_name)
        end

        private

        def check_for_wrong_save_request_example(node, body_param_name)
          success_node, arguments_hash = success_node_and_arguments_hash(node)

          return if success_node.nil?

          return add_missing_offense(success_node, body_param_name) if arguments_hash.empty?

          check_arguments_hash(success_node, body_param_name, arguments_hash.first)
        end

        def success_node_and_arguments_hash(node)
          node.children.each do |child|
            capture = success_response(child)

            return child, capture if capture
          end

          [nil, nil]
        end

        def check_arguments_hash(success_node, body_param_name, arguments_hash)
          save_request_example_name = save_request_example_name(arguments_hash)

          return add_missing_offense(success_node, body_param_name) if save_request_example_name.nil?

          return if save_request_example_name == body_param_name

          add_misnamed_offense(arguments_hash, body_param_name, save_request_example_name)
        end

        def success_response_code?(value)
          value.between?(200, 299)
        end

        def add_missing_offense(node, body_param_name)
          message = format(MISSING_MSG, param_name: body_param_name, current: source_location(node))
          add_offense(node.loc.expression, message: message)
        end

        def add_misnamed_offense(node, body_param_name, save_request_example_name)
          message = format(MISNAMED_MSG, wrong_param_name: save_request_example_name,
                                         correct_param_name: body_param_name,
                                         current: source_location(node))
          add_offense(node.loc.expression, message: message)
        end

        def source_location(node)
          range = node.location.expression
          path = smart_path(range.source_buffer.name)
          "#{path}:#{range.line}"
        end
      end
    end
  end
end
