# frozen_string_literal: true

module RuboCop
  module Cop
    module Iotventure
      # This cop checks for duplicated response code example definitions.
      #
      # @example
      #
      #   # bad
      #
      #   response 400, 'response description 1' {}
      #   response 400, 'response description 2' {}
      #
      #
      # @example
      #
      #   # bad
      #
      #   context 'context 1' do
      #     response 400, 'response description 1' {}
      #   end
      #   context 'context 2' do
      #     response 400, 'response description 2' {}
      #   end
      #
      # @example
      #
      #   # bad
      #
      #   2.times do
      #     response 400, 'response description' {}
      #   end
      #
      #
      # @example
      #
      #   # good
      #
      #   response 400, 'response description' do
      #     context 'context 1' {}
      #     context 'context 2' {}
      #   end
      #
      #
      # @example
      #
      #   # good
      #
      #   response 400, 'response description 1' {}
      #   response 401, 'response description 2' {}
      #
      class DuplicateResponseCode < Base
        DUPLICATE_MSG = 'Response Code `%<response_code>s` is defined at both %<other>s and %<current>s.'
        IN_LOOP_MSG = 'Response definition at %<current>s is defined inside loop at %<loop>s'

        # @!method response_code_definition?(node)
        def_node_matcher :response_code_definition, <<~PATTERN
          (block
            (send nil? :response
              (int $_)
              ...)
            ...
          )
        PATTERN

        # @!method route_definition?(node)
        def_node_matcher :route_definition, <<~PATTERN
          (block
            (send nil? #route_verb? (str _))
            ...
          )
        PATTERN

        def initialize(config = nil, options = nil)
          super
          @definitions = {}
        end

        def on_block(node)
          code = response_code_definition(node)
          return unless code

          check_for_duplicate_response_code(node, code)
          check_for_definition_inside_loop(node)
        end

        private

        def check_for_duplicate_response_code(node, response_code)
          route_definition_ancestor = route_definition_ancestor(node)
          return unless route_definition_ancestor

          route_definition_ancestor_line = route_definition_ancestor.first_line

          if (other_node = @definitions[route_definition_ancestor_line]&.[](response_code))
            message = message_for_dup(node, other_node, response_code)

            add_offense(node.loc.expression, message: message)
          else
            @definitions[route_definition_ancestor_line] ||= {}
            @definitions[route_definition_ancestor_line][response_code] = node
          end
        end

        def message_for_dup(node, other_node, response_code)
          format(DUPLICATE_MSG, response_code: response_code, other: source_location(other_node),
                                current: source_location(node))
        end

        def route_definition_ancestor(node)
          ancestor = node.parent
          ancestor = ancestor.parent until ancestor.nil? || route_definition(ancestor)
          ancestor
        end

        def route_verb?(word)
          %i[get post put patch delete].include? word.to_sym
        end

        def check_for_definition_inside_loop(node)
          ancestor = node
          until route_definition(ancestor)
            ancestor = ancestor.parent

            return if ancestor.nil?

            next unless loop_method?(ancestor)

            message = message_for_loop(node, ancestor)
            return add_offense(node.loc.expression, message: message)
          end
        end

        def loop_method?(node)
          return false unless node.block_type?

          send_node = node.send_node
          send_node.enumerable_method? || send_node.enumerator_method? || send_node.method?(:loop)
        end

        def message_for_loop(node, loop_node)
          format(IN_LOOP_MSG, current: source_location(node), loop: source_location(loop_node))
        end

        def source_location(node)
          range = node.location.expression
          path = smart_path(range.source_buffer.name)
          "#{path}:#{range.line}"
        end
      end
    end
  end
end
