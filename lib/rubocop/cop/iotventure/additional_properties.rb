# frozen_string_literal: true

module RuboCop
  module Cop
    module Iotventure
      # This cop checks that every array and object schema has additionalProperties (defined or set to false).
      # Note: This only works for schemas defined as Ruby hashes.
      #
      # @example
      #
      #   # bad
      #
      #     {
      #       type: :object,
      #       properties: {
      #         foo: { type: :string },
      #         bar: { type: :boolean }
      #       }
      #     }
      #   
      # @example
      #
      #   # bad
      #
      #     {
      #       type: :object,
      #       properties: {
      #         foo: { type: :string },
      #         bar: { type: :boolean }
      #       },
      #       additionalProperties: true
      #     }
      #
      #
      # @example
      #
      #   # good
      #
      #     {
      #       type: :object,
      #       properties: {
      #         foo: { type: :string },
      #         bar: { type: :boolean }
      #       },
      #       additionalProperties: false
      #     }
      #     
      #
      #
      class AdditionalProperties < Base
        MISSING_MSG = 'Schema is missing additionalProperties. ' \
                      'Please add it to the schema at %<current>s'
        ALWAYS_TRUE_MSG = 'Schema has additionalProperties set to true. ' \
                          'Please set it to false or define a rule at %<current>s'

        # @!method schema_definition_with_properties?(node)
        def_node_matcher :schema_object_definition_with_properties?, <<~PATTERN
          (hash<
            (pair
              (sym :type)
              {(sym :object) (str "object")}
            )
            (pair
              <{(sym :properties) (sym :patternProperties)}>
              ...
            )
            ...
          >)
        PATTERN

        # @!method always_true_schema?(node)
        def_node_matcher :always_true_schema?, <<~PATTERN
          {
            (true)
            (hash) # Empty hash is also always true in JSON Schema
          }
        PATTERN

        def on_hash(node)
          return unless schema_object_definition_with_properties?(node)

          additional_properties = node.pairs.find { |pair| pair.key.value == :additionalProperties }
          return add_missing_offense(node) unless additional_properties

          add_always_true_offense(node) if always_true_schema?(additional_properties.value)
        end

        private

        def add_missing_offense(node)
          add_offense(node, message: format(MISSING_MSG, current: source_location(node)))
        end

        def add_always_true_offense(node)
          add_offense(node, message: format(ALWAYS_TRUE_MSG, current: source_location(node)))
        end

        def source_location(node)
          range = node.location.expression
          path = smart_path(range.source_buffer.name)
          "#{path}:#{range.line}"
        end
      end
    end
  end
end
