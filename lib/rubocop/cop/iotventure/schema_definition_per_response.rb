# frozen_string_literal: true

module RuboCop
  module Cop
    module Iotventure
      # This cop checks that there is exactly one top-level schema definition per response declaration.
      #
      # @example
      #
      #   # bad
      #
      #   response 200, 'response description' {}
      #
      #
      # @example
      #
      #   # bad
      #
      #   response 200, 'response description' do
      #     context 'context' do
      #       schema '$ref' => '#/components/schemas/object'
      #     end
      #   end
      #
      #
      # @example
      #
      #   # bad
      #
      #   response 200, 'response description' do
      #       schema '$ref' => '#/components/schemas/object1'
      #       schema '$ref' => '#/components/schemas/object2'
      #   end
      #
      #
      # @example
      #
      #   # bad
      #
      #   schema '$ref' => '#/components/schemas/object'
      #   response 200, 'response description' {}
      #
      #
      # @example
      #
      #   # bad
      #
      #   response 204, 'response description' do
      #     schema '$ref' => '#/components/schemas/object'
      #   end
      #
      #
      # @example
      #
      #   # good
      #
      #   response 200, 'response description' do
      #     schema '$ref' => '#/components/schemas/object'
      #   end
      #
      #
      # @example
      #
      #   # good
      #
      #   response 204, 'response description' do
      #   end
      #
      #
      class SchemaDefinitionPerResponse < Base
        MISSING_MSG = 'Schema definition is missing for response declaration at %<current>s.'
        DUPLICATED_MSG = 'Schema definition is defined both at %<first>s and %<second>s '\
                         'for response declaration at %<current>s.'
        MISPLACED_MSG = 'Schema definition for %<schema_name>s is defined at %<current>s, '\
                        'but should be defined immediately after response declaration at %<other>s.'
        MISPLACED_WITHOUT_RESPONSE_DEFINITION_MSG = 'Schema definition for %<schema_name>s is '\
                                                    'outside of response declaration.'
        NO_CONTENT_SCHEMA_MSG = 'Schema definition for %<schema_name>s is defined at %<current>s, '\
                                'but 204 response should not have schema.'

        # @!method response_block?(node)
        def_node_matcher :response_block, <<~PATTERN
          (block
            (send nil? :response
              (int _)
              ...)
            ...
          )
        PATTERN

        # @!method no_content_response(node)
        def_node_matcher :no_content_response, <<~PATTERN
          (block
            (send nil? :response
              (:int 204)
              (:str _)
              ...
            )
            ...
          )
        PATTERN

        # @!method schema_definition?(node)
        def_node_matcher :schema_definition, <<~PATTERN
          (send nil? :schema
            (hash
              ...
            )
          )
        PATTERN

        # @!method schema_name(node)
        def_node_matcher :schema_name, <<~PATTERN
          (send nil? :schema
            (hash
              (pair
                (str "$ref")
                (str $_)
              )
            )
          )
        PATTERN

        # @!method schema_definition_by_child?(node)
        def_node_matcher :schema_definition_by_child, <<~PATTERN
          ({block | begin}
            <
              $(send nil? :schema
                (hash
                  ...
                )
              )
              ...
            >
          )
        PATTERN

        def on_block(node)
          return check_schema_definition_count(node) if response_block(node)

          return if begin_inside_response_block?(node)

          check_for_misplaced_schema(node)
        end
        alias on_begin on_block

        private

        # Checks that schema is missing completely. Does not check where schema is defined exactly,
        # that will be picked up by check_for_misplaced_schema
        def check_schema_definition_count(node)
          schema_definitions = schema_definitions(node)
          return check_no_schemas(schema_definitions) if no_content_response(node)

          return if schema_definitions.count == 1

          return add_missing_offense(node) if schema_definitions.empty?

          add_duplicated_offense(node, schema_definitions)
        end

        def check_no_schemas(schema_definitions)
          schema_definitions.each do |schema_definition|
            message = format(NO_CONTENT_SCHEMA_MSG, schema_name: find_schema_name(schema_definition),
                                                    current: source_location(schema_definition))
            add_offense(schema_definition.loc.expression, message: message)
          end
        end

        def add_missing_offense(node)
          message = format(MISSING_MSG, current: source_location(node))
          add_offense(node.loc.expression, message: message)
        end

        def add_duplicated_offense(node, schema_definitions)
          message = format(
            DUPLICATED_MSG,
            current: source_location(node),
            first: source_location(schema_definitions[0]),
            second: source_location(schema_definitions[1])
          )
          add_offense(node.loc.expression, message: message)
        end

        def check_for_misplaced_schema(node)
          schema_definition = schema_definition_by_child(node)
          return unless schema_definition

          correct_node = response_definition_ancestor(node)
          message = message_for_misplaced(schema_definition, correct_node)
          add_offense(schema_definition.loc.expression, message: message)
        end

        def message_for_misplaced(schema_definition, correct_node)
          if correct_node
            return format(MISPLACED_MSG,
                          schema_name: find_schema_name(schema_definition),
                          other: source_location(correct_node),
                          current: source_location(schema_definition))
          end

          format(MISPLACED_WITHOUT_RESPONSE_DEFINITION_MSG, schema_name: find_schema_name(schema_definition))
        end

        def begin_inside_response_block?(node)
          node.begin_type? && response_block(node.parent)
        end

        def schema_definitions(node)
          node.each_descendant.filter do |d|
            schema_definition(d)
          end
        end

        def response_definition_ancestor(node)
          ancestor = node.parent
          ancestor = ancestor.parent until ancestor.nil? || response_block(ancestor)
          ancestor
        end

        def find_schema_name(schema_definition)
          schema_name(schema_definition) || 'composite schema declaration'
        end

        def source_location(node)
          range = node.location.expression
          path = smart_path(range.source_buffer.name)
          "#{path}:#{range.line}"
        end
      end
    end
  end
end
