# Rubocop::Iotventure

This is a gem running some internal checks for IoTVenture. All of the existing cops are for RSwag, but that might change in the future.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rubocop-iotventure', require: false
```

And then execute:

```shell
bundle install
```

Or install it yourself using:

```shell
gem install rubocop-iotventure
```

## Usage

### IotVenture/AdditionalProperties

```ruby
# bad
{
    type: :object,
    properties: {
        prop1: { type: :string },
        prop2: { type: :string }
    }
}

# good
{
    type: :object,
    properties: {
        prop1: { type: :string },
        prop2: { type: :string }
    },
    additionalProperties: false
}
```

This cop checks that all object schemas have the `additionalProperties` property set to `false` or a useful matcher (not `true`). This is to prevent unexpected properties from being added to the object.

Note: This only works for schemas defined as Ruby hashes.

### Iotventure/DuplicateResponseCode

```ruby
# bad
response 400, 'response description 1' {}
response 400, 'response description 2' {}

# good
response 400, 'response description 1' {}
response 401, 'response description 2' {}
```

This cop prevents duplicated response code blocks. Those would overwrite each other when generating the `swagger.yaml` file.

### Iotventure/SaveRequestExample

```ruby
# bad
parameter name: :param1, in: :body
response 200, 'response description' {}

# good
parameter name: :param1, in: :body
response 200, 'response description', save_request_example: :param1 {}
```

This cop enforces usage of the `save_request_example` parameter in `api` files (saves the body parameter). This should only be enabled when there is custom logic reacting to this parameter.

### Iotventure/SchemaDefinitionPerResponse

```ruby
# bad
response 200, 'response description' do
    context 'context' do
        schema '$ref' => '#/components/schemas/object'
    end
end

# bad
schema '$ref' => '#/components/schemas/object'
response 200, 'response description' {}


# good
response 200, 'response description' do
    schema '$ref' => '#/components/schemas/object'
end
```

This cop checks that there is exactly one top-level schema definition per response block (except 204 No Content blocks, those should not have any schema definitions). Multiple or misplaced schema definitions might overwrite each other.

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Bitbucket at <https://bitbucket.org/iotventure/rubocop-iotventure>. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://bitbucket.org/iotventure/rubocop-iotventure/src/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Rubocop::Iotventure project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/iotventure/rubocop-iotventure/src/master/CODE_OF_CONDUCT.md).
