# frozen_string_literal: true

require_relative 'lib/rubocop/iotventure/version'

Gem::Specification.new do |spec|
  spec.name = 'rubocop-iotventure'
  spec.version = RuboCop::Iotventure::VERSION
  spec.authors = ['Fynn Starke']
  spec.email = ['f.starke@iot-venture.com']

  spec.summary = 'Rswag cops.'
  spec.description = 'Adds some helpful internal cops'
  spec.homepage = 'https://rubygems.org/gems/rubocop-iotventure'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.6.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/iotventure/rubocop-iotventure/src'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/iotventure/rubocop-iotventure/src/master/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'rubocop', '~> 1.0'
  spec.add_dependency 'rubocop-rake', '~> 0.6.0'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'simplecov'
  spec.metadata['rubygems_mfa_required'] = 'true'
end
